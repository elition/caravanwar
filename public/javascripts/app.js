/**
 * Check if the pattern of ID is valid
 */
checkId = () => {
  const regex = /^([a-z0-9]{4}-){4}[a-z0-9]{4}$/;
  var keyInput = document.getElementById("key");
  if (regex.test(keyInput.value)) {
    keyInput.className = "success";
  } else if (keyInput.value.length == 0) {
    keyInput.className = "empty";
  } else {
    keyInput.className = "error";
  }
}

// Get the modal
var modal = document.getElementById('selectSimulation');

// Get the button that opens the modal
var btn = document.getElementById("newSimulation");

// Get the <span> element that closes the modal
var span = document.getElementsByClassName("close")[0];

// When the user clicks the button, open the modal
btn.onclick = function () {
  modal.style.display = "flex";
}

// When the user clicks on <span> (x), close the modal
span.onclick = function () {
  modal.style.display = "none";
}

// When the user clicks anywhere outside of the modal, close it
window.onclick = function (event) {
  if (event.target == modal) {
    modal.style.display = "none";
  }
}