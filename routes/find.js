var express = require('express');
var router = express.Router();

/* GET home page. */
router.post('/', function(request, response, next) {
  console.log(request.body.id);
  response.render('attackSimulation', { title: 'Attack Simulation' });
});

module.exports = router;