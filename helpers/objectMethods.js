/**
 * Return True if at least one parameter is missing
 * @param {object} object
 * @param {array} array
 */
isParamMissing = (object, array) => {
  let arrayChecked = [];

  for (const param in object) {
    array.forEach(element => {
      if (element === param) {
        arrayChecked.push(element);
      }
    });
  }

  if (array.length === arrayChecked.length) {
    return false
  }

  return true;
}

/**
 * Return True if at least one parameter is null or empty
 * @param {object} object
 */
isParamEmpty = (object) => {
  for (const param in object) {
    if (object[param] === '' || object[param] == null) {
      return true;
    }
  }

  return false;
}

module.exports = { isParamMissing, isParamEmpty }