const bcrypt = require('bcrypt');
const config = require('../security/security');

/**
 * Generate an API key and return it
 * Example : a86a-84e4-c7fa-15c9-aa26
 */
generateApiKey = () => {
  var base = 'xxxx-xxxx-xxxx-xxxx-xxxx';
  var date = new Date().getTime();

  return base.replace(/[x]/g, function (c) {
    var r = (date + Math.random() * 16) % 16 | 0;
    date = Math.floor(date / 16);
    return r.toString(16);
  });
}

/**
 * Generate a simulation ID
 * Example : a86ap4
 */
generateSimulationId = () => {
  var base = 'xxxxxx';
  var date = new Date().getTime();

  return base.replace(/[x]/g, function (c) {
    var r = (date + Math.random() * 16) % 16 | 0;
    date = Math.floor(date / 16);
    return r.toString(16);
  });
}

/**
 * Password encryption
 * @param {string} password
 */
passwordEncryption = (password) => {
  return new Promise((resolve, reject) => {
    bcrypt.hash(password, config.security.saltRound, (error, encrypted) => {
      if (error) { reject(error); }
      resolve(encrypted);
    });
  });
}

/**
 * Check if password given match with the one in database
 * @param {string} password
 * @param {string} encryptedPassword
 */
checkPassword = (password, encryptedPassword) => {
  return new Promise((resolve, reject) => {
    bcrypt.compare(password, encryptedPassword, (error, response) => {
      if (error) { reject(error); }
      resolve(response);
    });
  });
}

module.exports = { generateApiKey, passwordEncryption, checkPassword }