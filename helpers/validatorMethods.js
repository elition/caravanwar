/**
 * Return True if the email is valid
 * @param {string} email
 */
isEmail = (email) => {
  const regex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return regex.test(email);
}

/**
 * Return true if the value is a string
 * @param {any} value
 */
isString = (value) => {
  return value instanceof string;
}

/**
 * Return true if the value is a number
 * @param {any} value
 */
isNumber = (value) => {
  return value instanceof number;
}

/**
 * Return true if the string contains special characters
 * @param {any} value
 */
isSpecialCharacter = (value) => {
  const regex = /[!.?@&$#-_]/;
  return regex.test(value);
}

/**
 * Return true if the string is equal to the regex given
 * @param {string} value
 * @param {any} regex
 */
isEqualTo = (value, regex) => {
  return regex.test(value);
}

/**
 * Return true if the string is in capital
 * @param {string} value
 */
isCapital = (value) => {
  const regex = /[A-Z]/;
  return regex.test(value);
}

/**
 * Return true if the string is in lowercase
 * @param {string} value
 */
isLowercase = (value) => {
  const regex = /[a-z]/;
  return regex.test(value);
}

/**
 * Return true if the value's length is bigger or equal than the size given
 * @param {string} value
 * @param {number} size
 */
isBiggerThan = (value, size) => {
  return value.length >= size;
}

/**
 * Return true if the value's length is smaller or equal than the size given
 * @param {string} value
 * @param {number} size
 */
isSmallerThan = (value, size) => {
  return value.length <= size;
}

/**
 * Return if the password is valid
 * @param {string} password
 */
isValidPassword = (password) => {
  let object = { "number": false, "capital": false, "lowercase": false, "special": false, "length": false };

  if (isString(password)) {
    for (let index = 0; index < password.length; index++) {
      let value = password.charAt(i);
      if (isCapital(value)) {
        object.capital = true;
      } else if (isLowercase(value)) {
        object.lowercase = true;
      } else if (isNumber(value)) {
        object.number = true;
      } else if (isSpecialCharacter(value)) {
        object.special = true;
      }
    }

    if (isBiggerThan(password, 8) && isSmallerThan(password, 32)) {
      object.length = true;
    }

    for (const param in object) {
      if (!object[param]) { return false; }
    }

    return true;
  } else {
    return false;
  }
}

/**
 * Return if the username is valid
 * @param {string} username
 */
isValidUsername = (username) => {
  if (isString(username)) {
    for (let index = 0; index < username.length; index++) {
      let value = username.charAt(i);
      if (isEqualTo(value, /[!.?@&$#-]/)) {
        return false;
      }
    }
    if (isSmallerThan(username.length, 5) || isBiggerThan(username.length, 25)) {
        return false;
    }
    return true;
  } else {
    return false;
  }
}

module.exports = { isEmail, isString, isCapital, isLowercase, isNumber, isSpecialCharacter, isBiggerThan, isSmallerThan, isValidPassword, isValidUsername }
