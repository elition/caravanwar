/**
 * Return the string given with the first letter in capital
 * Example : "capital" => "Capital"
 * @param {string} string
 */
capitalize = (string) => {
  return `${string.charAt(0).toUpperCase()}${string.substr(1)}`;
};

module.exports = { capitalize }