var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
const initDb = require('./db/mongo');

//! ROUTES
/* ---------------------------------------------------- */
var index = require('./routes/index');
var users = require('./routes/users');
var admin = require('./routes/admin');
var update = require('./routes/update');
var find = require('./routes/find');
var wiki = require('./routes/wiki');
/* ---------------------------------------------------- */
var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

//! USE
/* ---------------------------------------------------- */
app.use('/', index);
app.use('/users', users);
app.use('/admin', admin);
app.use('/update', update);
app.use('/find', find);
app.use('/wiki', wiki);
/* ---------------------------------------------------- */

initDb((db) => {
  global.db = db;

  db.createCollection('users', (err, res) => {
    if (err) throw err;
  });

  db.createCollection('simulations', (err, res) => {
    if (err) throw err;
  });

  db.createCollection('gardiens', (err, res) => {
    if (err) throw err;
  });

  db.createCollection('bandits', (err, res) => {
    if (err) throw err;
  });

  db.createCollection('vehicules', (err, res) => {
    if (err) throw err;
  });

  db.createCollection('modules', (err, res) => {
    if (err) throw err;
  });

  db.createCollection('sorts', (err, res) => {
    if (err) throw err;
  });

  db.createCollection('batiments', (err, res) => {
    if (err) throw err;
  });

  db.createCollection('tours', (err, res) => {
    if (err) throw err;
  });

  db.createCollection('campagnes', (err, res) => {
    if (err) throw err;
  });

  db.createCollection('ligues', (err, res) => {
    if (err) throw err;
  });

  db.createCollection('offres', (err, res) => {
    if (err) throw err;
  });

  // catch 404 and forward to error handler
  app.use(function(req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
  });

  // error handler
  app.use(function(err, req, res, next) {
    // set locals, only providing error in development
    res.locals.message = err.message;
    res.locals.error = req.app.get('env') === 'development' ? err : {};

    // render the error page
    res.status(err.status || 500);
    res.render('error');
  });
});

module.exports = app;
